# Introduction to Linux - LFS101

Introduction to **Linux** course.

## Getting started

This project it just to document my learning course [**Introduction to Linux (LFS101)**](https://www.edx.org/course/introduction-to-linux) in order to prepare for [**Essentials of Linux System Administration (LFS201)**](https://training.linuxfoundation.org/certification/essentials-of-linux-system-administration-lfs201-lfcs/) and **LFCS Exam**.

This project pretends to document the interest and basis content for future reference and to help others. If you are interested in taking this course you can do it for free in EDX platform: [**EDX Introduction to Linux**](https://www.edx.org/course/introduction-to-linux)

## Project status 

WIP

